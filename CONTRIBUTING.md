# Shell Configurator Extension Contributing Rules

## Developing

### If you want to contribute this extension, you need follow these rules:
* Use **simple, readable, and easy to understand** code
* **Indentation** must be **4 spaces** not tabs. We recommended using
  **Visual Studio Code** and configure indentation to 4 spaces for coding
* Use **underscore symbol** at the first character for private functions/method
  or variable
  - example: `_workspaceSwitcher`
* Between methods should be **separated** with **new line**
* Don't use **longer code**, at least **minimum of 250 characters**
* Inside of `ui` directory includes **ui file** such as preferences and widgets
* Inside of `library` directory includes **extension library**
* Inside of `library/modules` directory includes some component **modules** that
  combined (programmatically) into **`library/components.js`**
* **`stylesheet.css`** only used for **tuning/improving ui elements** to make
  shell ui more stable
* Use jsdoc (recommended) for documenting file


### If you want to add configurations with existing module, follow these instructions:
1.  Choose modules that you want to add at `library/modules/`*`module_name`*`.js`
2.  Type these codes below (recommended to use jsdoc for documentation):
```js
...
var ModuleName = class {
    ...
    backup() {
        ...
        this._origin = {
            ...
            'config': config // origin value from Main or other shell components
        }
    }

    restore() {
        ...
        this.configName(this._origin['config']);
        // Add another configurations method or method to restore/revert
        // component values/properties to origin
    }
    ...
    /**
     * Method Description
     * @returns {void}
     */
    configName() {
        // CODE GOES HERE
    }
    ...
}
```
3. Add configuration name to schema on
`schema/org.gnome.shell.extensions.shell-configurator`. Make sure it consistent
to commented module name (ex: search):
```xml
<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
  ...
  <schema id="org.gnome.shell.extensions.shell-configurator"
  path="/org/gnome/shell/extensions/shell-configurator/">
    ...
    <!-- Search --> <- example
    ...
    <key type="b" name="config-name">
      <default>true</default>
      <summary>Configuration Name</summary>
      <description>Tell configuration description</description>
    </key>
    ...
  </schema>
</schemalist>
```
4. Add configuration method on `library/manager.js`. Make sure it
consistent to commented module name (ex: search):
```js
...
var SCManager = class {
    ...
    _applyChanges() {
        ...
        // Search <- example
        ...
        this._search.configName(this._extensionSettings.get_boolean('config-name'));
        ...
    }
    ...
    _connectSignals() {
      this._signals = [
        ...
        // Search <- example
        [
            ...
            this._extensionSettings.connect('changed::config-name', () => {
                this._search.configName(this._extensionSettings.get_boolean('config-name'));
            })
        ],
        ...
      ];
    }
}
```
5. Add configuration each of presets (on presets folder):
```jsonc
{
  "name": "Default",
  ...
  "search": { // Example
    "config-name": true // Default value (default preset only and based on schema) or custom value
    ...
  }
  ...
}
```

**Rules:**
* Use function for configuration methods with comments to make methods easy to
  understand what this method going on
  - example: TODO


### If you want to add shell configuration module, follow these instructions:
1.  Create module to the `library/modules/`*`module_name`*`.js`, then copy/write
    these codes below (recommended to use jsdoc for documentation):
```js
/**
 * Module Name
 *
 * Describe this module
 *
 * @author      Advendra Deswanta <adeswanta@gmail.com>
 * @copyright   2021-2022
 * @license     GPL-3.0-only
 */

const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
    misc:Misc,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

const MODULE_ENUM = {
    ENUM0: 0,
    ENUM1: 1,
    ENUM2: 2
}

/**
 * Module Name
 *
 * Describe this module
 *
 * This Module can configure/do:
 * > Describe what this module can do
 */
var ModuleName = class extends template.ConfigurationModule {
    /**
     * Class constructor
     */
    constructor() {
        super();

        // GI libraries
        this._GIName = imports.gi.GIName || null;
        // Add another GI libraries

        // UI libraries
        this._LibraryName = imports.ui.libraryName || null;
        this._DirectElementName = this._Main.elementName || null;
        // Add another UI libraries

        // Conditional declarations
        this._ElementName =
            (this._shellVersion >= 40) ?
                this._Main.library.elementName
            :   this._Main.another.elementName;
        // Add another conditional declarations

        // Public declarations
        this.publicVar = true;
        // Add another private declarations

        // Private declarations
        this._privateVar = true;
        // Add another private declarations
    }

    /**
     * Backup shell properties
     * @returns {void}
     */
    backup() {
        super.backup();

        this._origin = {
            'configOrigin': originValue // origin value from Main or other shell components
            // Add another configurations properties with default shell value
        }
    }

    /**
     * Restore properties
     * @returns {void}
     */
    restore() {
        this.configMethodName(this._origin['configOrigin']);
        // Add another configurations method or method to restore/revert
        // component values/properties to origin
    }

    /**
     * Method Description
     * @param {*} param param description
     * @returns {void}
     */
    configMethodName(param) {
        // CODE GOES HERE
    }

    /**
     * Method Description
     * @returns {void}
     */
    _methodPrivate() {
        // CODE GOES HERE
    }
}
```
<small>*See `library/modules/_template.js` for references*</small>

3. Then, add module to schema on
`schema/org.gnome.shell.extensions.shell-configurator.gschema.xml`. Make sure it
consistent to commented module name (ex: search):
```xml
<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
  ...
  <schema id="org.gnome.shell.extensions.shell-configurator"
  path="/org/gnome/shell/extensions/shell-configurator/">
  ...
    <!-- Module Name -->
    ...
    <key type="b" name="config-name">
      <default>true</default>
      <summary>Configuration Name</summary>
      <description>Tell configuration description</description>
    </key>
    <!-- Add some configurations -->
    ...
  </schema>
</schemalist>
```
4. Add `ModuleName` to `library/components.js`:
```js
...
const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    ...
    moduleName:ModuleName
    ...
} = Self.imports.library.modules;
...
var SCComponents = class {
    constructor() {
        ...
        this.ModuleName = new ModuleName.ModuleName();
        ...
    }
    ...
    backup() {
        ...
        this.ModuleName.backup();
    }
    ...
    restore() {
        ...
        this.ModuleName.restore();
    }
}
```
5. Add following codes on `library/manager.js`. Make sure it consistent to
commented module name (ex: search):
```js
...
var SCManager = class {
    ...
    constructor(Self, Main, Settings, Libs, shellVersion) {
        ...
        // imports required library and components
        ...
        this._moduleName = this._Components.ModuleName;
    }
    ...
    destroy() {
        ...
        // Makes variables are null (in order not to make increasing memory)
        ...
        delete(this._moduleName);
        ...
    }
   ...
    _applyChanges() {
        ...
        // Module Name
        ...
        this._moduleName.configMethodName(this._extensionSettings.get_boolean('config-name'));
    }
    ...
    _connectSignals() {
        this._signals = [
            ...
            // ModuleName
            [
                this._extensionSettings.connect('changed::config-name', () => {
                    this._moduleName.configMethodName(this._extensionSettings.get_boolean('config-name'));
                }),
            ]
            ...
        ]
    }
    ...
}
```
6. Add configuration each of presets (on presets folder):
```jsonc
{
  "name": "Default",
  ...
  "search": { // Example
    "config-name": true // Default value (default preset only and based on schema) or custom value
    ...
  }
  ...
}
```
**Rules:**
* `this._Misc` is only used for helper or other methods on misc.js
* `this._shellVersion` is only used if your config is only available for certain
  shell compatibility cases
  - example case:
  * We want toget startSearch function and modify with custom methods, on
    GNOME 40 has a individual search control library called `searchControler`
    that includes the function on the class prototype, but GNOME 3.38 below
    doesn't have this library, instead it using combined libary on
    `viewSelector` which is also have this method (directly), so it needs a
    compatibility detection by typing following code here:
    ```js
    if (this._shellVersion >= 40) {
        // This scope for GNOME 40 above
        this.SarchController.SearchController.prototype.startSearch =
          function () {
              // Custom method
          }
    }
    else {
        // This scope for GNOME 3.38 below
        this.ViewSelector.startSearch = function () {
            // Custom method
        }
    }

    ```
  * [OLD CASES] The Main top panel actor can be accessed with `Main.panel` but
    on GNOME 3.36 below only be accessed with `Main.panel.actor`, so you need
    compatibility detection by typing following code:
    ```js
    if (this._shellVersion >= 3.36)
        this._panel = this._Main.panel;
    else
        this._panel = this._Main.panel.actor;
    ```
    or use ternary operator (if it have only one condition):
    ```js
    this.SearchControler =
        (this._shellVersion >= 40) ?
            this._Main.panel
        :   this._Main.panel.actor;
    ```


### If you want to create another libraries, follow these instructions:
1.  Create module to the library/*library_name*.js, then copy/write these codes
    below (recommended to use jsdoc for documentation):
```js
/**
 * Library Name
 *
 * Library description
 *
 * @author      Advendra Deswanta <adeswanta@gmail.com>
 * @copyright   2021-2022
 * @license     GPL-3.0-only
 */

// The rest of the code
```


### For text/variable naming cases use these cases:
* **CapitalCase** for Shell Configurator (SC) library
  - example: Components and UI library variables; library name: components.js
* **camelCase** for functions/methods, variables, and object properties
  - example: workspaceSwitcherScaleSize
* **PascalCase/CapitalCamelCase** for classes
  - example: SearchEntry


## Translating

### If you want to make this extension available for all/your language(s), you need follow these rules:
* You can check the main template file in `template.pot` on `locale` folder. That is the reference of the translation
* Initial language translation for current languages has been translated via online services. Thanks to [Pot-Translator](https://github.com/smarteist/Pot-Translator) to translate all languages automatically from Google Translate. Feel free to edit that.
* You can use [GTranslator](https://gitlab.gnome.org/GNOME/gtranslator) or any translation editor to edit the translation file