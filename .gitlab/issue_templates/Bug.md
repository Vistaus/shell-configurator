## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Current bug behavior

(What actually happens)

## Expected correct behavior

(What you should see/be instead)

## Other enabled extensions

(What extensions are installed and enabled beside this extension and what is
happening when all extensions are disabled and only this extension is enabled)

## Relevant logs, screenshots and/or screencasts

(For logs, run `journalctl -f` (for system/gjs logs) or `journalctl -fo cat
/usr/bin/gnome-shell` (for shell logs) - please use code blocks (```) to format
console output, logs, and code, For screenshot use "GNOME Screenshot" or any
other screenshot provider. For screencast use "OBS Studio", "GNOME Screencast",
or any other screencast provider)

### **Details**

**GNOME Shell Version:**
*Your current gnome-shell version. You can check by running `gnome-shell --version` command*

**Extension Version:** *You can check by opening extension preferences, and you will see the extension version*

**Extension Install method:**
*Where is the extension is installed, Extension Website, GitLab Releases, or Build from Source?*